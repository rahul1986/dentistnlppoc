import csv

from django.conf import settings
from django.core.management.base import BaseCommand

from data_crawler.views import CrawlYellowPage


class Command(BaseCommand):

    """
    Management command to crawl Yellow Pages reviews for a category in US.
    The command will fetch Zip codes from CSV file and crawl data for each zip for the
    category provided in settings.
    """

    def handle(self, *args, **options):

        zip_list = []
        file_path = "{0}/data_files/zip_codes.csv".format(settings.BASE_DIR)

        with open(file_path, 'r') as csv_file:
            csv_obj = csv.reader(csv_file)
            for row in csv_obj:
                zip_list.append(str(row[0]))
        for each in zip_list:
            print("Crawling data for ZIP {0}".format(each))
            CrawlYellowPage(each, category=settings.FETCH_CATEGORY)
            print("Crawled data for ZIP {0}".format(each))

        print("Crawled reviews from yellowpages.com")
