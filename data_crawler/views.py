import requests

from bs4 import BeautifulSoup
from django.conf import settings
from ratelimit import limits, sleep_and_retry


class CrawlYelp(object):

    """
    This class will crawl reviews from yelp.com for given category and zip code with a rate limit of 2 call per minute.
    The crawled reviews will be stored in <category>_review_yelp.txt file with blank line after each review.
    """

    CRAWL_URL = "https://www.yelp.com/search?find_desc={0}&find_loc={1}&start={2}"

    def __init__(self, zip_code, category=None):

        """
        Function to initialize class.

        Args:
            zip_code: Zip code for which data need to be parsed.
            category: Category for which data need to be parsed
        Returns:
            The file "<category>_reviews_yelp.txt" in data_files folder with crawled reviews.
        """

        self.category = category
        self.zip_code = zip_code
        self.start_index = 0
        file_path = "{0}/data_files/{1}_review_yelp.txt".format(settings.BASE_DIR, self.category)
        self.fo = open(file_path.format(self.category), "a")
        self.get_business_list()
        self.fo.close()

    @sleep_and_retry
    @limits(calls=2, period=60)
    def call_api(self, url):

        """
        Function to call api with 2 requests/minute. This param can be changed in the decorator.

        Args:
            url: URequested URL.
        Returns:
            complete response of request made.
        Raises:
            Exception: if response status code is not 200(OK).
        """

        response = requests.get(url)
        if response.status_code != 200:
            raise Exception('Cannot call API: {}'.format(response.status_code))
        return response

    def fetch_html(self, parse_review=True):

        """
        Function to parse HTML. Process and write reviews in the file.

        Args:
            parse_review: Flag to check if reviews need to be parsed or not. By default, set to True.
        Returns:
            If parse_review : True flag else: BeautifulSoup object of the parsed URL.
        """

        url = self.CRAWL_URL.format(self.category, self.zip_code, self.start_index)
        response = self.call_api(url)
        html_soup = BeautifulSoup(response.text, 'html.parser')
        if parse_review:
            data_list_html = html_soup.find_all('li', class_='regular-search-result')

            for each in data_list_html:
                start = 0
                end = 19
                review_count_data = each.find('span', class_='review-count rating-qualifier')

                if review_count_data:
                    review_count_str = review_count_data.text
                    try:
                        review_count = int(review_count_str.replace(' ', '').split("review")[0].strip('\n'))
                    except ValueError:
                        review_count = 0

                    if review_count <= 20:
                        if review_count > 0:
                            self.parse_review(each.a.get('href'), start)
                    else:
                        while review_count > end + 1:
                            self.parse_review(each.a.get('href'), start)
                            start = end + 1
                            end += 20
        if not parse_review:
            return html_soup
        else:
            return True

    def parse_review(self, url, offset):

        """
        Function to parse reviews from detail URL and write in the file.

        Args:
            url: YELP detail page Href url.
            offset: Start index of the reviews.
        """

        detail_href = "https://www.yelp.com{0}?start={1}".format(url, offset)
        detail_response = self.call_api(detail_href)
        detail_html_soup = BeautifulSoup(detail_response.text, 'html.parser')
        review_list = detail_html_soup.find('div', class_="review-list")

        if review_list:
            reviews = review_list.find_all('div', class_="review-content")
            for review in reviews:
                review_content = review.find('p').text
                self.fo.write(review_content)
                self.fo.write("\n\n")

    def get_business_list(self):

        """
        Function which will be called from __init__() function when class object is created.
        This function will fetch, parse and write reviews for each zip code in the file.
        Returns:
            True, when process is completed
        """

        count = 10
        html_soup = self.fetch_html(parse_review=False)

        if self.start_index == 0:
            count_container = html_soup.find_all('span', class_='pagination-results-window')
            if count_container:
                data_count_div = count_container[0]
                count_str = data_count_div.text
                count = int(count_str.replace(' ', '').split("of")[1].rstrip('\n'))

        while self.start_index < count:
            self.fetch_html()
            self.start_index += 10
        return True


class CrawlYellowPage(object):

    """
    This class will crawl reviews from yelp.com for given category and zip code with a rate limit of 5 call per minute.
    The crawled reviews will be stored in <category>_review_yp.txt file with blank line after each review.
    """

    CRAWL_URL = "https://www.yellowpages.com/search?search_terms={0}&geo_location_terms={1}&page={2}"

    def __init__(self, zip_code, category=None):

        """
        Function to initialize class.

        Args:
            zip_code: Zip code for which data need to be parsed.
            category: Category for which data need to be parsed
        Returns:
            The file "<category>_reviews_yelp.txt" in data_files folder with crawled reviews.
        """

        self.category = category
        self.zip_code = zip_code
        self.start_index = 0
        file_path = "{0}/data_files/{1}_reviews_yp.txt".format(settings.BASE_DIR, self.category)
        self.fo = open(file_path.format(self.category), "a")
        self.get_business_list()
        self.fo.close()

    @sleep_and_retry
    @limits(calls=5, period=60)
    def call_api(self, url):

        """
        Function to call api with 5 requests/minute. This param can be changed in the decorator.

        Args:
            url: URequested URL.
        Returns:
            complete response of request made.
        Raises:
            Exception: if response status code is not 200(OK).
        """

        response = requests.get(url)
        if response.status_code != 200:
            raise Exception('Cannot call API: {}'.format(response.status_code))

        return response

    def fetch_html(self, parse_review=True):

        """
        Function to parse HTML. Process and write reviews in the file.

        Args:
            parse_review: Flag to check if reviews need to be parsed or not. By default, set to True.
        Returns:
            If parse_review : True flag else: BeautifulSoup object of the parsed URL.
        """

        url = self.CRAWL_URL.format(self.category, self.zip_code, self.start_index)
        response = self.call_api(url)
        html_soup = BeautifulSoup(response.text, 'html.parser')
        if parse_review:
            data_list_html = html_soup.find_all('div', class_='result')

            for each in data_list_html:

                if len(each.get('class')) == 1 and each.get('class')[0] == 'result':
                    if each.find('a', attrs={'class': 'rating'}):
                        page_num = 1
                        rev_index = 0
                        detail_href = each.find('a', attrs={'class': 'rating'}).get('href')
                        review_cnt = each.find('span', attrs={'class': 'count'}).text[1:-1]
                        biz_id = detail_href.split("?lid=")[1].split("#")[0]

                        while rev_index < int(review_cnt):
                            review_href = "https://www.yellowpages.com/listings/{0}/" \
                                          "reviews?page={1}".format(biz_id, page_num)
                            rev_index += 20
                            page_num += 1

                            detail_response = self.call_api(review_href)
                            detail_html_soup = BeautifulSoup(detail_response.text, 'html.parser')
                            review_list = detail_html_soup.findAll('article')

                            for review in review_list:
                                review_content = review.find('div', attrs={"class": "review-response"}).p.text
                                self.fo.write(review_content)
                                self.fo.write("\n\n")
        if not parse_review:
            return html_soup
        else:
            return True

    def get_business_list(self):

        """
        Function which will be called from __init__() function when class object is created.
        This function will fetch, parse and write reviews for each zip code in the file.
        Returns:
            True, when process is completed
        """

        count = 30
        html_soup = self.fetch_html(parse_review=False)
        if self.start_index == 0:
            count_container = html_soup.find_all('div', class_='pagination')
            if count_container:
                data_count_div = count_container[0]
                count_str = data_count_div.text
                count = int(count_str.split('found')[1].split('results')[0])
        pages = int(count/30)
        if count % 30 != 0:
            pages += 1

        while self.start_index < pages:
            self.start_index += 1
            self.fetch_html()
        return True
