# Dentist NLP POC

This project is to crawl reviews for particular category from Yelp and Yellowpages for US
zip codes and then run nlp on those reviews to fetch sentiment and confidence interval of the review and save in database.

## Getting Started

The project has been build on python3.6 and Mysql 5.7. After setting up the project run requirement.txt to install required packages.


### Prerequisites

```
Mysql 5.7
Python 3.6
Zip code CSV of US
```

### Installing

Below are the list of step which will help you in running the project

Create virtual environment
```
virtualenv -p python3.6 env
```

Install required packages

```
pip install -r requirement.txt
```

Run migrations

```
python manage.py migrate
```

Crawl yelp (This step can be overridden if you have crawled data.)

```
python manage.py crawl_yelp
```

Crawl yellowpages (This step can be overridden if you have crawled data.)

```
python manage.py crawl_yp
```

Process crawled reviews

```
python manage.py process_data
```

## Built With

* [Python3.6](https://docs.python.org/3/) - Programming language
* [Django2](https://docs.djangoproject.com/en/2.0/) - The web framework used
* [BeautifulSoup4](https://readthedocs.org/projects/beautiful-soup-4) - HTML Parser used
* [Nltk](https://www.nltk.org/) - Natural language toolkit
* [Ratelimit](https://pypi.org/project/ratelimit/) - Used to threshold the requests

## Version Control

We use [Git](https://bitbucket.org) for version control.

## Authors

* **Rahul Agarwal** - *Initial work* - [rahul1986](https://bitbucket.org/rahul1986)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

