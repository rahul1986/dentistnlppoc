from django.db import models


class Review(models.Model):

    """
    Model definition to store a single review entry.
    """

    SENTIMENT_CHOICE = (
        (1, "Positive"),
        (0, "Neutral"),
        (-1, "Negative"),
    )

    review = models.TextField("Review", blank=True)
    snippet = models.TextField("Snippet", blank=True)
    keywords = models.TextField("Review keywords", blank=True, help_text="Comma separated list")
    sentiment = models.IntegerField("Review Sentiment", choices=SENTIMENT_CHOICE, blank=True, null=True)
    confidence_interval = models.FloatField("Confidence Interval", null=True, blank=True)
    adjectives = models.TextField("Review adjectives", blank=True, help_text="Comma separated list")

    def __unicode__(self):
        return self.review

    class Meta:

        ordering = ['id']
        verbose_name = "review"
        verbose_name_plural = "reviews"
