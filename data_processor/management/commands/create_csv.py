import csv

from django.core.management.base import BaseCommand
from django.conf import settings
from data_processor.models import Review


class Command(BaseCommand):

    """
    Management command to create CSV of processed reviews.
    Read reviews from the database and append the review in CSV file using '|' as delimiter.
    """

    def handle(self, *args, **options):

        rev_obj = []
        review_header = ["Review", "Snippet", "Adjectives", "Keywords", "Sentiment", "Confidence Interval"]
        review_file_path = "{0}/data_files/{1}".format(settings.BASE_DIR, "reviews.csv")

        reviews_list = Review.objects.all()
        for review in reviews_list:
            rev_data = list()
            rev_data.append(review.review)
            rev_data.append(review.snippet if review.snippet else "")
            rev_data.append(review.adjectives)
            rev_data.append(review.keywords)
            if review.sentiment == -1:
                sentiment = "Negative"
            elif review.sentiment == 1:
                sentiment = "Positive"
            else:
                sentiment = "Neutral"
            rev_data.append(sentiment)
            rev_data.append(review.confidence_interval)
            rev_obj.append(rev_data)

        with open(review_file_path, 'w', newline='') as csv_file:
            review_writer = csv.writer(csv_file, delimiter='|', quoting=csv.QUOTE_MINIMAL)
            review_writer.writerow(review_header)
            for rev in rev_obj:
                review_writer.writerow(rev)

        print("CSV created for reviews and can be found at {0}".format(review_file_path))
