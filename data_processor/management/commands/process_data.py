from django.conf import settings
from django.core.management.base import BaseCommand
from data_processor.views import ProcessReview, ReviewKeywordsIdentifier, GetSnippet,\
    GetConfidenceInterval, GetSentiment


class Command(BaseCommand):

    """
    Management command to process crawled data from text file.
    Read the files, and parse data on blank line.
    Initialize each review in ProcessReview class and save the review and corresponding adjectives.
    Find snippet for each review using GetSnippet class.
    Find keywords for each review using ReviewKeywordsIdentifier class.
    Find sentiment for each review using GetSentiment class.
    Find confidence interval for each review using GetConfidenceInterval class.
    After all steps are completed one can view detail per review in admin section.
    """

    def handle(self, *args, **options):

        file_lists = ['{0}_reviews_yp.txt'.format(settings.FETCH_CATEGORY),
                      '{0}_reviews_yelp.txt'.format(settings.FETCH_CATEGORY)
                      ]

        for file_name in file_lists:

            file_path = "{0}/data_files/{1}".format(settings.BASE_DIR, file_name)
            with open(file_path, 'r') as reviewFile:

                review_str = ""

                for line in reviewFile:
                    review_str += line
                    if line == "\n" and review_str != "":
                        review_str = review_str.replace("\n", " ").strip()
                        ProcessReview(review_str)
                        review_str = ""
            print("Processed data for file {0}".format(file_name))

        GetSnippet()
        print("Identified snippet for each review.")

        ReviewKeywordsIdentifier()
        print("Identified keywords for each review.")

        GetSentiment()
        print("Identified sentiment for each review.")

        GetConfidenceInterval()
        print("Identified confidence interval for each review.")

        print("Review processing has been completed.")