from django.apps import AppConfig


class DataProcesserConfig(AppConfig):
    name = 'data_processor'
