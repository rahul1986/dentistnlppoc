from django.contrib import admin
from data_processor.models import Review


class ReviewAdmin(admin.ModelAdmin):

    """
    Admin class to define backend UI for Review model.
    """

    list_display = ['id', 'sentiment', 'confidence_interval']
    list_filter = ["sentiment"]
    search_fields = ["review", "keywords", "snippet"]


admin.site.register(Review, ReviewAdmin)
