import collections
import nltk
import string

from itertools import chain, groupby, product
from collections import Counter, defaultdict
from django.conf import settings
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from data_processor.models import Review
from nltk.corpus import sentiwordnet as swn
from nltk.tokenize import wordpunct_tokenize
from functools import reduce

sid = SentimentIntensityAnalyzer()
wnl = nltk.stem.WordNetLemmatizer()
stop_words = nltk.corpus.stopwords.words('english')
punctuation = string.punctuation
ignore_words = set(chain(stop_words, punctuation))


class ReviewBaseClass(object):

    """
    Base class containing all private methods for sub classes.
    """

    def __init__(self):
        self.review = ""

    @staticmethod
    def _get_reviews_objects():

        """
        Function to get the list of all the reviews.

        Returns:
            Review list(Queryset).
        """

        return Review.objects.all()

    def _get_reviews_count(self):

        """
        Function to get the count of all the reviews.

        Returns:
            Review count(int).
        """

        return self._get_reviews_objects().count()

    def _set_review_to_class(self, review):

        """
        Set review to class object

        Returns:
            None.
        """

        self.review = review

    def _get_tokenize_list(self):

        """
        Get tokenize list of the review

        Returns:
            Tokenize list of words(list).
        """

        return nltk.word_tokenize(self.review)

    def _remove_ignore_words(self):

        """
        Function to remove stop words and punctuations from tokenize list

        Returns:
            Tokenize list of words after removing ignored words.
        """

        tokenize_list = self._get_tokenize_list()
        return [w.lower() for w in tokenize_list if w.lower() not in ignore_words]

    def _get_lemmatize_set(self):

        """
        Function to get the tag for each word

        Returns:
            Tagged tuple list of word.
        """

        lem_token = [wnl.lemmatize(token) for token in self._remove_ignore_words()]
        return set(nltk.pos_tag(lem_token))

    def _get_n_grams(self, gram_num):

        """
        Set review to class object
        Args:
            gram_num<int> : No of grams need to be calculated
        Returns:
            N gram collection set.
        """

        n_grams = nltk.ngrams(self._remove_ignore_words(), gram_num)
        return collections.Counter(n_grams)

    def _get_sentence_list(self):
        return nltk.tokenize.sent_tokenize(self.review)

    @staticmethod
    def _get_word_list(sentence):
        return [word.lower() for word in wordpunct_tokenize(sentence)]


class ProcessReview(ReviewBaseClass):

    """
    This class will take review string(assuming review is in english) as an init param.
    Find the list of adjectives in the review, update the review if exist or save the new one.
    """

    def __init__(self, review):

        """
        Initialize class function to process review.

        Args:
            review: review need to be processed and saved in the database.
        Returns:
            None.
        """

        super(ProcessReview, self).__init__()
        self.review = str(review).strip()
        self.process_review_str()

    def process_review_str(self):

        """
        Base function which will be called from __init__() function to process the review.

        Returns:
            None.
        Raises:
            Exception: if review could not be saved in the database.
        """

        adjective = list()

        lem_token_list = self._get_lemmatize_set()
        for each in lem_token_list:
            if each[1] in ['JJ', 'JJR', 'JJS']:
                adjective.append(each[0])
        adj_str = ", ".join(adjective)

        try:
            Review.objects.update_or_create(review=self.review, defaults={"adjectives": adj_str})
        except Exception as e:
            print("Exception while creating object {0} for review: \n".format(e))
            print(self.review)


class GetSnippet(ReviewBaseClass):

    """
    Class to find the snippet for the review. For eah review call function fetch_snippet
    Distribute review into sentences and find the compound probability of each sentence.
    The sentence with highest compound probability or lowest compound probability
    will be the snippet for the review.
    Save the returned snipped for the review in database.
    """

    def __init__(self):

        """
        Initialize class function to get snippet from the review.

        Returns:
             None.
        """

        super(GetSnippet, self).__init__()

        for review in self._get_reviews_objects():
            self.review = review.review
            self.fetch_snippet()
            snippet = self.fetch_snippet()
            review.snippet = snippet
            review.save()

    def fetch_snippet(self):

        """
        Function to find the snippet for each review.

        Returns:
            snippet<str>: Snippet for the review.
        """

        sentences = self._get_sentence_list()
        snippet_pos = snippet_neg = ""
        pop_score_pos = pop_score_neg = 0

        for sent in sentences:
            pop_dict = sid.polarity_scores(sent)
            if pop_dict.get("compound") > 0 and pop_dict.get("compound") > pop_score_pos:
                pop_score_pos = pop_dict.get("compound")
                snippet_pos = sent
            if pop_dict.get("compound") < 0 and pop_dict.get("compound") < pop_score_neg:
                pop_score_neg = pop_dict.get("compound")
                snippet_neg = sent

        if pop_score_pos > -pop_score_neg:
            return snippet_pos
        else:
            return snippet_neg


class ReviewKeywordsIdentifier(ReviewBaseClass):

    """
    Class to find the keyword for the review. For each review call function fetch_keyword.
    Distribute review into sentences and find word list for each sentence.
    Create contender phrases from the list of words that form a sentence.
    Builds frequency distribution of the words in the review.
    Builds the co-occurrence graph of words in the review to compute degree of each word.
    Rank each contender phrase using the formula
              phrase_score = sum of scores of words in the phrase.
              word_score = d(w)/f(w) where d is degree and f is frequency
    Based on settings.STAT_SCORE find keywords for the review greater than the score.
    Save the returned keywords string for the review in database.
    """

    def __init__(self):

        """
        Initialize class function to identify keyword reviews.

        Returns:
             None.
        """
        super(ReviewKeywordsIdentifier, self).__init__()

        for review in self._get_reviews_objects():
            self.review = review.review
            keywords = self.fetch_keyword()
            review.keywords = keywords
            review.save()

    def fetch_keyword(self):

        """
        Function to find the keywords for each review.

        Returns:
            keywords<string>: comma separated string of keywords for the review.
        """
        phrase_list = set()
        keyword_list = list()
        co_occurrence_graph = defaultdict(lambda: defaultdict(lambda: 0))
        degree = defaultdict(lambda: 0)

        sentences = self._get_sentence_list()
        for sentence in sentences:
            word_list = self._get_word_list(sentence)
            groups = groupby(word_list, lambda x: x not in ignore_words)
            phase_data = [tuple(group[1]) for group in groups if group[0]]
            phrase_list.update(phase_data)
        frequency_distributor = Counter(chain.from_iterable(phrase_list))

        for phrase in phrase_list:
            for (word, co_word) in product(phrase, phrase):
                co_occurrence_graph[word][co_word] += 1

        for key in co_occurrence_graph:
            degree[key] = sum(co_occurrence_graph[key].values())
        rank_list = list()
        for phrase in phrase_list:
            rank = 0.0
            for word in phrase:
                rank += 1.0 * degree[word] / frequency_distributor[word]

            rank_list.append((rank, " ".join(phrase)))
        rank_list.sort(reverse=True)
        for keyword_rank in rank_list:
            if keyword_rank[0] > settings.STAT_SCORE:
                keyword_list.append(keyword_rank[1])
        return ", ".join(keyword_list)


class GetSentiment(ReviewBaseClass):

    """
    Class to find the Sentiment for the review. For each review call function fetch_sentiment.
    Distribute review into sentences and distribute each sentence to words list.
    For tagged list of word in sentence find word score using Sentiment Intensity Analyser.
    Calculate average score for each sentence, and then average score of the review.
    Based on average score assign sentiment to the review.
    Save the returned sentiment for the review in database.
    """

    def __init__(self):

        """
        Initialize class function to identify sentiment for the review.

        Returns:
             None.
        """
        super(GetSentiment, self).__init__()
        for review in self._get_reviews_objects():
            self.review = review.review
            sentiment = self.fetch_sentiment()
            review.sentiment = sentiment
            review.save()

    def fetch_sentiment(self):

        """
        Function to find the sentiment for each review.

        Returns:
            sentiment<int>: sentiment for the review.
        """

        score_list = list()
        sentence_sentiment = list()

        sentences = self._get_sentence_list()
        words_list = [self._get_word_list(sentence) for sentence in sentences]
        tagged_list = [nltk.pos_tag(word_list) for word_list in words_list]

        for idx, tagged_sent in enumerate(tagged_list):
            score_list.append([])

            for idx2, t in enumerate(tagged_sent):
                new_tag = ''
                lem_token = wnl.lemmatize(t[0])
                if t[1].startswith('NN'):
                    new_tag = 'n'
                elif t[1].startswith('JJ'):
                    new_tag = 'a'
                elif t[1].startswith('V'):
                    new_tag = 'v'
                elif t[1].startswith('R'):
                    new_tag = 'r'

                if new_tag != '':
                    syn_sets = list(swn.senti_synsets(lem_token, new_tag))
                    score = 0
                    if len(syn_sets) > 0:
                        for syn in syn_sets:
                            score += syn.pos_score() - syn.neg_score()
                        score_list[idx].append(score / len(syn_sets))

        for score_sent in score_list:
            if score_sent:
                sentence_sentiment.append(reduce(lambda x, y: x + y, score_sent) / len(score_sent))
            else:
                sentence_sentiment.append(0)

        if sentence_sentiment:
            rev_sentiment = reduce(lambda x, y: x + y, sentence_sentiment) / len(sentence_sentiment)
        else:
            rev_sentiment = 0
        if rev_sentiment > 0:
            sentiment = 1
        elif rev_sentiment < 0:
            sentiment = -1
        else:
            sentiment = 0
        return sentiment


class GetConfidenceInterval(ReviewBaseClass):

    """
    Class to find the Confidence Interval for the review. For each review call function fetch_conf_interval.
    Generate vocab list. Distribute reviews in set of settings.CI_DIST variable and train each set.
    Classify each train set using NaiveBayesClassifier classifier.
    For each reviews based on each trained set find the probability distribution using classifier.
    Find the probability of sentiment on the basis of probability distribution for each trained set.
    Save the returned confidence interval for the review in database.
    """

    def __init__(self):

        """
        Initialize class function to identify sentiment for the review.

        Returns:
             None.
        """
        super(GetConfidenceInterval, self).__init__()
        self.distributed_sent_dict = dict()
        word_list = list()
        for review in self._get_reviews_objects():
            self.review = review.review
            word_list.extend(self._remove_ignore_words())

        self.word_net = list(set(word_list))

        offset = int(self._get_reviews_count() / settings.CI_DIST)
        start_index = 0
        end_index = offset
        distributed_list = list()
        while end_index <= self._get_reviews_count():
            distributed_list.append(self._get_reviews_objects()[start_index: end_index])
            start_index = end_index
            end_index += offset
        for idx, rev_list in enumerate(distributed_list):
            train_set = self.create_training_set(rev_list)
            classifier = nltk.classify.NaiveBayesClassifier.train(train_set)
            self.distributed_sent_dict[idx] = classifier
        for review in self._get_reviews_objects():
            self.review = review.review
            conf_interval = self.fetch_conf_interval(review.sentiment)
            review.confidence_interval = conf_interval
            review.save()

    def create_training_set(self, data_list):

        """
        Function to create training set.
        Args:
            data_list: List of reviews for which training set need to be created.
        Returns:
            train_set: Trained set for reviews subset.
        """
        train_set = list()
        for review in data_list:
            data_dict = dict.fromkeys(self.word_net, 0)
            self.review = review.review
            tokenize_words = self._remove_ignore_words()
            data_dict.update(dict.fromkeys(tokenize_words, 1))
            train_set.append((data_dict, review.sentiment))
        return train_set

    def fetch_conf_interval(self, sentiment):

        """
        Function to find the confidence interval for each review.

        Returns:
            confidence_interval<float>: confidence interval for the review.
        """

        rev_prob_list = list()
        data_dict = dict.fromkeys(self.word_net, 0)
        tokenize_words = self._remove_ignore_words()
        data_dict.update(dict.fromkeys(tokenize_words, 1))

        for idx, classifier in enumerate(self.distributed_sent_dict.values()):
            prob_dist = classifier.prob_classify(data_dict)
            if prob_dist.prob(1) > prob_dist.prob(0) and prob_dist.prob(1) > prob_dist.prob(-1):
                rev_prob_list.append(1)
            elif prob_dist.prob(-1) > prob_dist.prob(0) and prob_dist.prob(-1) > prob_dist.prob(1):
                rev_prob_list.append(-1)
            else:
                rev_prob_list.append(0)

        conf_interval = (rev_prob_list.count(sentiment) / len(rev_prob_list)) * 100
        return conf_interval
