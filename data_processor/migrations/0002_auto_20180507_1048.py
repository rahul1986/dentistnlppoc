# Generated by Django 2.0.4 on 2018-05-07 10:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data_processor', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='keyworddistributor',
            options={'ordering': ['keyword'], 'verbose_name': 'keyword', 'verbose_name_plural': 'keywords'},
        ),
        migrations.AlterModelOptions(
            name='reviewdistributor',
            options={'ordering': ['review'], 'verbose_name': 'review', 'verbose_name_plural': 'reviews'},
        ),
        migrations.RemoveField(
            model_name='reviewdistributor',
            name='sentiment_score',
        ),
        migrations.AlterField(
            model_name='keyworddistributor',
            name='keyword',
            field=models.CharField(blank=True, max_length=100, verbose_name='Keyword'),
        ),
        migrations.AlterField(
            model_name='keyworddistributor',
            name='reviews_occurrence',
            field=models.IntegerField(default=0, help_text='Number of reviews in which keyword occur', verbose_name='No of reviews'),
        ),
        migrations.AlterField(
            model_name='keyworddistributor',
            name='total_occurrence',
            field=models.IntegerField(default=0, help_text='Total number of occurrence of the keyword', verbose_name='Total count'),
        ),
        migrations.AlterField(
            model_name='reviewdistributor',
            name='adjectives',
            field=models.TextField(blank=True, help_text='Comma separated list', verbose_name='Review adjectives'),
        ),
        migrations.AlterField(
            model_name='reviewdistributor',
            name='confidence_interval',
            field=models.FloatField(blank=True, null=True, verbose_name='Confidence Interval'),
        ),
        migrations.AlterField(
            model_name='reviewdistributor',
            name='keywords',
            field=models.TextField(blank=True, help_text='Comma separated list', verbose_name='Review keywords'),
        ),
        migrations.AlterField(
            model_name='reviewdistributor',
            name='review',
            field=models.TextField(blank=True, verbose_name='Review'),
        ),
        migrations.AlterField(
            model_name='reviewdistributor',
            name='sentiment',
            field=models.IntegerField(choices=[(1, 'Positive'), (0, 'Neutral'), (-1, 'Negative')], default=0, verbose_name='Review Sentiment'),
        ),
        migrations.AlterField(
            model_name='reviewdistributor',
            name='snippet',
            field=models.TextField(blank=True, verbose_name='Snippet'),
        ),
        migrations.AlterField(
            model_name='reviewdistributor',
            name='word_count',
            field=models.TextField(blank=True, help_text='JSON data of tokenize set of words', verbose_name='Word count'),
        ),
    ]
